import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PeopleComponent } from './people/people.component';
import { PlanetsComponent } from './planets/planets.component';
import { FilmsComponent } from './films/films.component';
import { SpeciesComponent } from './species/species.component';
import { VehiclesComponent } from './vehicles/vehicles.component';
import { StarshipsComponent } from './starships/starships.component';
import { MenuBarComponent } from './menu-bar/menu-bar.component';
import { PersonDetailComponent } from './person-detail/person-detail.component';
import { PeopleServiceService } from './common/people-service.service';
import { FilmDetailComponent } from './film-detail/film-detail.component';
import { FilmService } from './common/film.service';
import { PlanetDetailComponent } from './planet-detail/planet-detail.component';
import { PlanetService } from './common/planet.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PeopleComponent,
    PlanetsComponent,
    FilmsComponent,
    SpeciesComponent,
    VehiclesComponent,
    StarshipsComponent,
    MenuBarComponent,
    PersonDetailComponent,
    FilmDetailComponent,
    PlanetDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [PeopleServiceService,FilmService,PlanetService],
  bootstrap: [AppComponent]
})
export class AppModule { }
