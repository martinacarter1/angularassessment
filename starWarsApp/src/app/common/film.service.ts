import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoggerService } from './logger.service';


@Injectable({
  providedIn: 'root'
})
export class Film{
  constructor(
  public title: string,
  public episode_id: string,
  public director: string,
  public producer: string,
  public release_date:string,
  public characters:Array<string>){}
}
export class FilmService {

  constructor(private httpClient:HttpClient,
    private log: LoggerService ) { }
getFilms(): Observable<any> {
    this.log.log('FilmService', `returning a collection of films`)
    return this.httpClient.get<any>('https://swapi.co/api/films/')
}
}
