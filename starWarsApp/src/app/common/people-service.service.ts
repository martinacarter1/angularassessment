import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoggerService } from './logger.service';

@Injectable({
  providedIn: 'root'
})
export class Person {
  constructor(
      public name: string,
      public height: string,
      public mass: string,
      public hair_color: string,
      public eye_color: string,
      public birth_year:string,
      public gender: string,
      public homeworld: string,
      public films: Array<string>,
      public species: Array<string>,
      public vehicles: Array<string>,
      public starships: Array<string>,
      public created:string,
      public edited: string,
      public url: string) {
  }
}
export class PeopleServiceService {

  constructor(private httpClient:HttpClient,
    private log: LoggerService ) { }
getPeople(): Observable<any> {
    this.log.log('PersonService', `returning a collection of people`)
    return this.httpClient.get<any>('https://swapi.co/api/people/')
}

}


