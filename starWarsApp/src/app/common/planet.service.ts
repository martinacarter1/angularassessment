import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoggerService } from './logger.service';

@Injectable({
  providedIn: 'root'
})
export class Planet{
  constructor(
    public name:string,
    public rotation_period:string,
    public orbital_period:string,
    public diameter:string,
    public climate:string,
    public terrain:string,
    public surface_water:string,
    public population:string
  ){}
}
export class PlanetService {

  constructor(private httpClient:HttpClient,
    private log: LoggerService ) { }
getPlanets(): Observable<any> {
    this.log.log('PlanetService', `returning a collection of planets`)
    return this.httpClient.get<any>('https://swapi.co/api/planets/')
}
}
