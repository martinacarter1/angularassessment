import { Component, OnInit, Input } from '@angular/core';
import { Film } from '../common/film.service';
import { LoggerService } from '../common/logger.service';

@Component({
  selector: 'app-film-detail',
  templateUrl: './film-detail.component.html',
  styleUrls: ['./film-detail.component.css']
})
export class FilmDetailComponent implements OnInit {
  @Input() film:Film;
  constructor(private log: LoggerService) { }

  ngOnInit() {
    this.log.log('FilmDetailComponent', `Added file ${this.film.title}`);

  }

}
