import { Component, OnInit } from '@angular/core';
import { FilmService } from '../common/film.service';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {


  films:any;
 
  constructor(private fs:FilmService) { 
    fs.getFilms().subscribe(response =>{
      console.log("People: "+JSON.stringify(response.results));
      this.films = response.results;
      
    });
  }

  ngOnInit() {
  }

}
