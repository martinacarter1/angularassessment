import { Component, OnInit, Input } from '@angular/core';
import { Person,PeopleServiceService } from '../common/people-service.service';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit {

  people:any;
 
  constructor(private ps:PeopleServiceService) { 
    ps.getPeople().subscribe(response =>{
      console.log("People: "+JSON.stringify(response.results));
      this.people = response.results;
      
    });
  }
  

  ngOnInit() {
  }

}
