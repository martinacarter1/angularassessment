import { Component, OnInit, Input } from '@angular/core';
import { Person } from '../common/people-service.service';
import { LoggerService } from '../common/logger.service';

@Component({
  selector: 'app-person-detail',
  templateUrl: './person-detail.component.html',
  styleUrls: ['./person-detail.component.css']
})
export class PersonDetailComponent implements OnInit {
  @Input() person: Person;
  constructor(private log:LoggerService) { }

  ngOnInit() {
    this.log.log('PersondetailComponent', `Added file ${this.person}`);
  }

}
