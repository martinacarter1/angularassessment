import { Component, OnInit, Input } from '@angular/core';
import { Planet } from '../common/planet.service';
import { LoggerService } from '../common/logger.service';
@Component({
  selector: 'app-planet-detail',
  templateUrl: './planet-detail.component.html',
  styleUrls: ['./planet-detail.component.css']
})
export class PlanetDetailComponent implements OnInit {
  @Input() planet: Planet;
  constructor(private log:LoggerService) { }

  ngOnInit() {
    this.log.log('PlanetDetailComponent', `Added file ${this.planet}`);

  }

}
