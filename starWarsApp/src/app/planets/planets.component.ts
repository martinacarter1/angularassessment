import { Component, OnInit } from '@angular/core';
import { PlanetService } from '../common/planet.service';

@Component({
  selector: 'app-planets',
  templateUrl: './planets.component.html',
  styleUrls: ['./planets.component.css']
})
export class PlanetsComponent implements OnInit {
  planets:any;
 
  constructor(private ps:PlanetService) { 
    ps.getPlanets().subscribe(response =>{
      console.log("Planet: "+JSON.stringify(response.results));
      this.planets = response.results;
      
    });
  }
  

  ngOnInit() {
  }

}
